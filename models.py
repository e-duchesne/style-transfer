#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: etienne
"""

import keras.backend as K
from loss_and_utils import sq_distance, gram_loss


class GramModel:
    """Model for style transfer using gramian prior"""

    def __init__(self, model, content_img, style_img, content_layers=None,
                 style_layers=None, alpha=0.001):

        self.alpha = alpha

        self.layers = dict([(layer.name, layer) for layer in model.layers])
        self.input = model.input

        # initialize content-related layers
        for layer in content_layers:
            assert layer in self.layers
        self.content_layers = content_layers

        self.content_input = content_img
        self.content_output = {}
        for layer in self.content_layers:
            f_content = K.function([self.input], self.layers[layer].output)
            self.content_output[layer] = f_content([self.content_input])

        # initialize style-related layers
        for layer in style_layers:
            assert layer in self.layers
        self.style_layers = style_layers

        self.style_input = style_img
        self.style_output = {}
        for layer in self.style_layers:
            f_style = K.function([self.input], self.layers[layer].output)
            self.style_output[layer] = f_style([self.style_input])

        # generate the loss and grad functions for optimization
        self.loss, self.grad = None, None
        self.generate_loss_grad()

    def generate_loss_grad(self):

        loss = K.variable(0.)

        for layer in self.content_layers:
            loss += sq_distance(self.layers[layer].output, self.content_output[layer])
        loss /= len(self.content_layers)
        for layer in self.style_layers:
            cur_layer = self.layers[layer]
            shape = self.style_output[layer].shape
            size = shape[1]*shape[2]
            loss += self.alpha*gram_loss(cur_layer.output, self.style_output[layer], size, shape[3])
        loss /= len(self.style_layers)

        grad = K.gradients(loss, [self.input])

        self.loss = lambda x: K.function([self.input], loss)([x])

        self.grad = lambda x: K.function([self.input], grad[0])([x])
