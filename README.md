# Style transfer

This is an implementation using Keras of
[Image Style Transfer Using Convolutional Neural Networks](http://www.cv-foundation.org/openaccess/content_cvpr_2016/html/Gatys_Image_Style_Transfer_CVPR_2016_paper.html)


* `loss_and_utils.py` contains the loss functions needed
* `models.py` contains the classe implementing
the loss and grad functions
* `artistic_style_transfer.py` is a script
performing the optimization on the content and style images given in
argument

The classic example of Van Gogh's 'Starry night' style
transferred to a view of Bristol harbour obtained with `artistic_style_transfer.py`:

<img src="http://e-duchesne.gitlab.io/test-results/bristol_small.jpg" height="200">
<img src="http://e-duchesne.gitlab.io/test-results/Van_Gogh_-_Starry_Night.jpg" height="200">
<img src="http://e-duchesne.gitlab.io/test-results/bristol_style_transfer.jpg" height="200">
