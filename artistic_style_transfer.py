#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: etienne

exemple of use:
python artistic_style_transfer.py content.jpg style.jpg -m vgg19 --content_layers block4_conv2 \
       --style_layers block1_conv1 block2_conv1 block3_conv1 block4_conv1 block5_conv1 --prefix result

"""
import keras.backend as K
import keras.applications as app
import numpy as np
from scipy.ndimage import zoom, imread
from scipy.misc import imsave
import argparse
from scipy.optimize import minimize
from loss_and_utils import preprocessing, postprocessing, gen_bounds
from models import GramModel

K.set_learning_phase(0)

parser = argparse.ArgumentParser()
parser.add_argument('content_img')
parser.add_argument('style_img')
parser.add_argument('--init_img', default=None)
parser.add_argument('--prefix', default='result-artistic-transfer')
parser.add_argument('--model', '-m', choices=['vgg16', 'vgg19', 'resnet50', 'inceptionV3'], default='vgg16')
parser.add_argument('--content_layers', nargs='*', default=['block4_conv2'])
parser.add_argument('--style_layers', nargs='*', default=['block1_conv1','block2_conv1','block3_conv1','block4_conv1','block5_conv1'])
parser.add_argument('--alpha', default=1000, type=float)
parser.add_argument('--it', default=200, type=int)
args = parser.parse_args()

if args.model == 'vgg16':
    model = app.vgg16.VGG16(include_top=False)
elif args.model == 'vgg19':
    model = app.vgg19.VGG19(include_top=False)
elif args.model == 'resnet50':
    model = app.resnet50.ResNet50(include_top=False)
else:
    model = app.inception_v3.InceptionV3(include_top=False)

content_layers = args.content_layers
style_layers = args.style_layers

iterations = args.it
prefix = args.prefix
alpha = args.alpha

content_img = preprocessing(imread(args.content_img))
content = content_img.astype(K.floatx())
shape_c = content_img.shape

if args.init_img is not None:
    x0 = preprocessing(imread(args.init_img))
    shape_x0 = x0.shape
    x0 = zoom(x0, (1, shape_c[1] / shape_x0[1], shape_c[2] / shape_x0[2], 1)).astype(K.floatx())
    x0 = x0
else:
    x0 = np.random.uniform(0, 255, size=shape_c[1:])
    x0 = preprocessing(x0).astype(K.floatx())
    shape_x0 = x0.shape
    x0 = x0

# Todo: support for 'channels_first'
assert K.image_data_format() == 'channels_last'
assert K.backend() == 'theano', 'Currently requiring theano for theano.tensor.nnet.neighbours.images2neibs()'

bounds = gen_bounds(shape_x0[1:])

style_img = preprocessing(imread(args.style_img))
shape_s = style_img.shape
style_resized = zoom(style_img, (1, shape_c[1]/shape_s[1], shape_c[2]/shape_s[2], 1))
style = style_resized.astype(K.floatx())

transfer = GramModel(model, content, style,
                     content_layers=content_layers, style_layers=style_layers,
                     alpha=alpha)
res = minimize(
    lambda x: transfer.loss(x.reshape(shape_c)),
    x0.flatten(),
    jac=lambda x: transfer.grad(x.reshape(shape_c)).flatten(),
    method='L-BFGS-B',
    bounds=bounds,
    options={'maxiter': iterations, 'disp': True})
x0 = res.x.reshape(shape_c)
img_res = postprocessing(x0)
imsave(prefix + '.jpg', img_res)
