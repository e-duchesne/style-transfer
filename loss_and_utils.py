#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: etienne duchesne
"""

from keras.layers import Lambda
import keras.backend as K
import numpy as np
from theano.tensor.nnet.neighbours import images2neibs
from keras.applications.imagenet_utils import preprocess_input


def total_variation(img, beta=1):
    """total variation regularizer"""

    du = K.square(img[0, 1:, :-1, :] - img[0, :-1, :-1, :])
    dv = K.square(img[0, :-1, 1:, :] - img[0, :-1, :-1, :])
    reg = K.sum(K.pow(du + dv, beta))

    return reg


def sq_distance(a, b):
    """squared distance between vectors a and b"""

    dist = K.sum(K.square(a - b))

    return dist


def nni(features, style_patches):
    """nearest neighbor index"""

    res = K.argmax(normalized_cross_correlation(features, style_patches))
    return res


def mrf_loss(features, style_patches, patches_volume):
    """compute the tensors containing the MRF loss"""

    h, w, filters = patches_volume
    idx_best_match = nni(features, style_patches)

    features_patches = create_patches(features, patches_volume)
    style_patches = K.variable(style_patches)

    loss = sq_distance(features_patches, style_patches[idx_best_match])/(h * w)

    return loss


def create_patches(features, patches_volume):

    h, w, filters = patches_volume

    patches = \
        K.permute_dimensions(
            K.reshape(
                images2neibs(features.dimshuffle(0, 3, 1, 2), (3, 3), (1, 1)),
                (filters, -1, h, w)),
            (1, 2, 3, 0)
        )

    return patches


def cross_correlation(features, style_patches):

    patches_nb, _, _, filters = style_patches.shape

    # weights from style patches used in the convolution
    # (flipping axis to compute cross-correlation as a convolution)
    weights = K.variable(style_patches[:, ::-1, ::-1, :].transpose((1, 2, 3, 0)))

    # cross correlation of patches
    cross_corr = Lambda(lambda x: K.conv2d(x, weights))(features)

    return cross_corr


def normalized_cross_correlation(features, style_patches):

    patches_nb, _, _, filters = style_patches.shape

    # L2 norms of style patches for normalization
    norm_style_patches = K.sum(K.square(style_patches.transpose((1, 2, 3, 0))),
                               axis=(0, 1, 2),
                               keepdims=True)

    # L2 norms of content patches for normalization
    norm_content_patches = \
        K.expand_dims(
            K.squeeze(
                K.conv2d(
                    K.square(features),
                    K.ones(shape=(3, 3, filters, 1))), 3),
            axis=3)

    # cross correlation of patches
    cross_corr = cross_correlation(features, style_patches)

    # normalization of cross correlation
    norm_cross_corr = cross_corr / K.sqrt(norm_content_patches * norm_style_patches)

    # reshaping as (content_patches_nb, style_patches_nb)
    norm_cross_corr = K.reshape(norm_cross_corr, (-1, patches_nb))

    return norm_cross_corr


def gramian(m):
    """return the Gram matrice of columns in m"""

    g = K.dot(K.transpose(m), m)
    return g


def gram_loss(gen_features, style_features, size, filters):

    gen_features = K.reshape(gen_features, (size, filters))
    style_features = K.reshape(style_features, (size, filters))
    loss = sq_distance(gramian(gen_features), gramian(style_features))
    loss /= (size**2)*(filters**2)
    return loss


def preprocessing(img):

    preproc_img = np.copy(img)
    preproc_img = np.expand_dims(preproc_img, axis=0)
    preproc_img = preprocess_input(preproc_img.astype(K.floatx()))
    return preproc_img


def postprocessing(img):

    postproc_img = np.copy(img)

    postproc_img[:, :, :, 0] += 103.939
    postproc_img[:, :, :, 1] += 116.779
    postproc_img[:, :, :, 2] += 123.68

    # BGR -> RGB
    postproc_img = postproc_img[:, :, :, ::-1]

    postproc_img = postproc_img.astype('uint8')
    postproc_img = np.clip(postproc_img, 0, 255)
    return postproc_img[0]


def gen_bounds(size):
    """generate min and max bounds for the l-bfgs-b optimization"""

    low = np.zeros(size)
    low = preprocessing(low)
    high = 255*np.ones(size)
    high = preprocessing(high)
    bounds = list(zip(low.flatten(), high.flatten()))
    return bounds
